package it.unibo.oop.lab.reflection01;

import org.junit.Test;

/**
 * Used to test reflection on ClonableClass.
 * 
 */
public class TestingCloner {

    /**
     * Used to test reflection on a ClonableClass instance.
     * 
     */
    @Test
    public void testInstanceCloning() {
        /*
         * 1) Creare una istanza di ClonableClass
         * 
         * 2) Invocare i vari setter per memorizzare dei valori nei campi
         * 
         * 3) Clonare l'oggetto usando il metodo cloneObj di ObjectClonerUtil
         * 
         * 4) verificare che i campi dell'oggetto clonato coincidano a quelli
         * dell'oggetto di partenza
         */
    }
}
